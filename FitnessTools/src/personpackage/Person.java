package personpackage;
/*
 * Abstract class person, inherits MalePerson and FemalePerson
 */
public abstract class Person {
	private String name;
	private double weight;
	private double height;
	private int age;
	private double bodyFatPercentage;
	private double leanBodyMass;
	private double caloriesMaintainWeight;
	
	public double getBodyFatPercentage() {
		return bodyFatPercentage;
	}

	public void setBodyFatPercentage(double bodyFatPercentage) {
		this.bodyFatPercentage = bodyFatPercentage;
	}
	
	public abstract void setCaloriesMaintainWeight(double lbm); 
		/*double restingMetabolicRate = lbm * 10;
		double caloricModificationRecovery = restingMetabolicRate * .3;
		double restingMetabolicRateModifiedForRecovery = caloricModificationRecovery +
				restingMetabolicRate;
		caloriesMaintainWeight = restingMetabolicRateModifiedForRecovery + 600;*/
	
	
	public double getCaloriesMaintainWeight() {
		
		return caloriesMaintainWeight;
	}
	
	public abstract String toString();

	
}
