import personpackage.*;
import caloriecalculator.*;

public class FitnessToolsMain {
	public static void main(String[] args) {
		CalorieCalculator c = new CalorieCalculator();
		//MalePerson juan = new MalePerson("Juan", 27, 155, 17, 170);
		MalePerson rafa = new MalePerson("Rafa", 26, 151.45, 19.2, 173);
		System.out.println(rafa);
		
		//System.out.println(rafa.getLeanBodyMass() + " " + rafa.getCaloriesMaintainWeight());
		c.setCaloriesNeededToBulk(rafa);
		c.setCaloriesNeededToCut(rafa);
		System.out.println(c.getCaloriesNeededToBulk());
		System.out.println(c.getCaloriesNeededToCut());
	}
}
