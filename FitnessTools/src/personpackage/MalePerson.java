package personpackage;

public class MalePerson extends Person {
	private String name;
	private double weight;
	private double height;
	private int age;
	private double bodyFatPercentage;
	private double leanBodyMass;
	private double caloriesMaintainWeight;
	
	public MalePerson(String name, int age, double weight, double bodyFatPercentage, double height) {
		this.name = name;
		this.age = age;
		this.weight = weight;
		setBodyFatPercentage(bodyFatPercentage);
		this.height = height;
		setLeanBodyMass(weight, bodyFatPercentage);
	}
	
	public double getBodyFatPercentage() {
		return bodyFatPercentage;
	}

	public void setBodyFatPercentage(double bodyFatPercentage) {
		this.bodyFatPercentage = bodyFatPercentage;
	}

	public void setLeanBodyMass(double weight, double bodyFatPercentage) {
		this.leanBodyMass = (((100 - bodyFatPercentage) / 100) * weight);
		setCaloriesMaintainWeight(leanBodyMass);
	}
	
	public double getLeanBodyMass() {
		return leanBodyMass;
	}



	public void setCaloriesMaintainWeight(double lbm) {
		double restingMetabolicRate = lbm * 10;
		double caloricModificationRecovery = restingMetabolicRate * .3;
		double restingMetabolicRateModifiedForRecovery = caloricModificationRecovery +
			   restingMetabolicRate;
		caloriesMaintainWeight = restingMetabolicRateModifiedForRecovery + 600;
	}
	
	public double getCaloriesMaintainWeight() {
		
		return caloriesMaintainWeight;
	}
	
	public String toString() { 
	    return "Name: '" + this.name + "', Height: '" + this.height;
	} 
}
