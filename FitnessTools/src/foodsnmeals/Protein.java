package foodsnmeals;

public class Protein implements MacroNutrient{
	private int grams;
	final int MULTIPLIER = 4;
	
	public void setGrams(int g) {
		this.grams = g;
	}
	
	public int getGrams() {
		return grams;
	}
	@Override
	public void calories(int c) {
		c = grams * MULTIPLIER;
		
	}

}
