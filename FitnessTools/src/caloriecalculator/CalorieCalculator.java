package caloriecalculator;
import personpackage.*;

public class CalorieCalculator {
	private double caloriesNeededToBulk;
	private double caloriesNeededToCut;
	
	public void setCaloriesNeededToBulk(Person p) {
		if (p.getBodyFatPercentage() >= 20) { 
			caloriesNeededToBulk = p.getCaloriesMaintainWeight() + 
							      (p.getCaloriesMaintainWeight() * .10); 
		}
		if (p.getBodyFatPercentage() < 20 && p.getBodyFatPercentage() > 10) {
			caloriesNeededToBulk = p.getCaloriesMaintainWeight() + 
							      (p.getCaloriesMaintainWeight() * .15); 
		} else {
			caloriesNeededToBulk = p.getCaloriesMaintainWeight() + 
								  (p.getCaloriesMaintainWeight() * .20); 
		}
	}
	
	public double getCaloriesNeededToBulk() {
		return caloriesNeededToBulk;
	}

	public double getCaloriesNeededToCut() {
		return caloriesNeededToCut;
	}

	public void setCaloriesNeededToCut(Person p) {
		if (p.getBodyFatPercentage() >= 20) { 
			caloriesNeededToCut = p.getCaloriesMaintainWeight() - 
							      (p.getCaloriesMaintainWeight() * .20); 
		}
		if (p.getBodyFatPercentage() < 20 && p.getBodyFatPercentage() > 10) {
			caloriesNeededToCut = p.getCaloriesMaintainWeight() -
								   (p.getCaloriesMaintainWeight() * .15); 
		} else {
			caloriesNeededToCut = p.getCaloriesMaintainWeight() -
		                           (p.getCaloriesMaintainWeight() * .10); 
		}
	}
	
	
}